﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Diagnostics;

namespace KuckuQuiz
{
    class Categories
    {
        private string foldername;
        private List<string> categories;
        private bool success;
        private string handle;
        private Logger logger = new Logger(3, "KuckuQuiz_Question");

        public Categories(string foldername)
        {
            try
            {
                var client = new WebClient();
                string categories_text = client.DownloadString("http://www.michaelkuckuk.com/de/KuckuCode/KuckuQuiz/themen.txt");
                StreamWriter categories = File.CreateText(foldername + "/themen.txt");
                categories.Write(categories_text);
                categories.Close();
            }
            catch
            {
                logger.Warning("categories konnten nicht gedownloadet werden!");
            }
            try
            {
                using (StreamReader categoriesInput = new StreamReader(foldername + "/themen.txt"))
                {
                    while (!categoriesInput.EndOfStream)
                    {
                        categories.Add(categoriesInput.ReadLine());
                    }
                }
                success = true;
                logger.Info("categories wurden ausgelesen");
            }
            catch
            {
                logger.Error("categories wurden nicht ausgelesen!");
                success = false;
            }
        }

        public bool Success
        {
            get
            {
                return this.success;
            }
        }

        public bool Next(int index)
        {
            bool result = false;
            try
            {
                this.handle = this.categories[index];
                this.categories.RemoveAt(index);
                result = true;
            }
            catch(IndexOutOfRangeException)
            {
                logger.Error("IndexOutOfRange!!!!! thema mit index " + index + "existiert nicht!");
                result = false;
            }
            return result;
        }

        public string Handle
        {
            get
            {
                return this.handle;
            }
        }
    }
}