﻿namespace KuckuQuiz
{
    partial class QuizDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btAnswerA = new System.Windows.Forms.Button();
            this.btAnswerB = new System.Windows.Forms.Button();
            this.btAnswerC = new System.Windows.Forms.Button();
            this.btAnswerD = new System.Windows.Forms.Button();
            this.lbQuestion = new System.Windows.Forms.Label();
            this.tbUsername = new System.Windows.Forms.TextBox();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.btSendLogin = new System.Windows.Forms.Button();
            this.listBoxChallenges = new System.Windows.Forms.ListBox();
            this.lbChallenges = new System.Windows.Forms.Label();
            this.lbForPointsUser = new System.Windows.Forms.Label();
            this.lbPointsUser = new System.Windows.Forms.Label();
            this.lbForPointsOpponent = new System.Windows.Forms.Label();
            this.lbPointsOpponent = new System.Windows.Forms.Label();
            this.btSendPlay = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btAnswerA
            // 
            this.btAnswerA.Location = new System.Drawing.Point(295, 56);
            this.btAnswerA.Name = "btAnswerA";
            this.btAnswerA.Size = new System.Drawing.Size(301, 259);
            this.btAnswerA.TabIndex = 0;
            this.btAnswerA.Text = "A";
            this.btAnswerA.UseVisualStyleBackColor = true;
            // 
            // btAnswerB
            // 
            this.btAnswerB.Location = new System.Drawing.Point(602, 56);
            this.btAnswerB.Name = "btAnswerB";
            this.btAnswerB.Size = new System.Drawing.Size(301, 259);
            this.btAnswerB.TabIndex = 1;
            this.btAnswerB.Text = "B";
            this.btAnswerB.UseVisualStyleBackColor = true;
            // 
            // btAnswerC
            // 
            this.btAnswerC.Location = new System.Drawing.Point(295, 321);
            this.btAnswerC.Name = "btAnswerC";
            this.btAnswerC.Size = new System.Drawing.Size(301, 259);
            this.btAnswerC.TabIndex = 2;
            this.btAnswerC.Text = "C";
            this.btAnswerC.UseVisualStyleBackColor = true;
            // 
            // btAnswerD
            // 
            this.btAnswerD.Location = new System.Drawing.Point(602, 321);
            this.btAnswerD.Name = "btAnswerD";
            this.btAnswerD.Size = new System.Drawing.Size(301, 259);
            this.btAnswerD.TabIndex = 3;
            this.btAnswerD.Text = "D";
            this.btAnswerD.UseVisualStyleBackColor = true;
            // 
            // lbQuestion
            // 
            this.lbQuestion.AutoSize = true;
            this.lbQuestion.Location = new System.Drawing.Point(292, 9);
            this.lbQuestion.Name = "lbQuestion";
            this.lbQuestion.Size = new System.Drawing.Size(294, 13);
            this.lbQuestion.TabIndex = 4;
            this.lbQuestion.Text = "Informatik: Wie spricht man die Programmiersprache C# aus?";
            // 
            // tbUsername
            // 
            this.tbUsername.Location = new System.Drawing.Point(13, 56);
            this.tbUsername.MaxLength = 50;
            this.tbUsername.Name = "tbUsername";
            this.tbUsername.Size = new System.Drawing.Size(54, 20);
            this.tbUsername.TabIndex = 5;
            this.tbUsername.Text = "Username";
            this.tbUsername.Click += new System.EventHandler(this.tbUsername_Click);
            this.tbUsername.Leave += new System.EventHandler(this.tbUsername_Leave);
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(73, 56);
            this.tbPassword.MaxLength = 50;
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.Size = new System.Drawing.Size(54, 20);
            this.tbPassword.TabIndex = 6;
            this.tbPassword.Text = "Passwort";
            this.tbPassword.Click += new System.EventHandler(this.tbPassword_Click);
            this.tbPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPassword_KeyPress);
            this.tbPassword.Leave += new System.EventHandler(this.tbPassword_Leave);
            // 
            // btSendLogin
            // 
            this.btSendLogin.Location = new System.Drawing.Point(133, 54);
            this.btSendLogin.Name = "btSendLogin";
            this.btSendLogin.Size = new System.Drawing.Size(75, 23);
            this.btSendLogin.TabIndex = 7;
            this.btSendLogin.Text = "Einloggen!";
            this.btSendLogin.UseVisualStyleBackColor = true;
            // 
            // listBoxChallenges
            // 
            this.listBoxChallenges.FormattingEnabled = true;
            this.listBoxChallenges.Items.AddRange(new object[] {
            "LBBO",
            "mayoren",
            "test1",
            "test2",
            "test3",
            "test4",
            "test5",
            "test6",
            "test7",
            "test8",
            "test9"});
            this.listBoxChallenges.Location = new System.Drawing.Point(12, 127);
            this.listBoxChallenges.Name = "listBoxChallenges";
            this.listBoxChallenges.Size = new System.Drawing.Size(120, 95);
            this.listBoxChallenges.TabIndex = 8;
            this.listBoxChallenges.Enter += new System.EventHandler(this.listBoxChallenges_Enter);
            // 
            // lbChallenges
            // 
            this.lbChallenges.AutoSize = true;
            this.lbChallenges.Location = new System.Drawing.Point(13, 111);
            this.lbChallenges.Name = "lbChallenges";
            this.lbChallenges.Size = new System.Drawing.Size(40, 13);
            this.lbChallenges.TabIndex = 9;
            this.lbChallenges.Text = "Duelle:";
            // 
            // lbForPointsUser
            // 
            this.lbForPointsUser.AutoSize = true;
            this.lbForPointsUser.Location = new System.Drawing.Point(12, 253);
            this.lbForPointsUser.Name = "lbForPointsUser";
            this.lbForPointsUser.Size = new System.Drawing.Size(75, 13);
            this.lbForPointsUser.TabIndex = 10;
            this.lbForPointsUser.Text = "Deine Punkte:";
            // 
            // lbPointsUser
            // 
            this.lbPointsUser.AutoSize = true;
            this.lbPointsUser.Location = new System.Drawing.Point(114, 253);
            this.lbPointsUser.Name = "lbPointsUser";
            this.lbPointsUser.Size = new System.Drawing.Size(41, 13);
            this.lbPointsUser.TabIndex = 11;
            this.lbPointsUser.Text = "Punkte";
            // 
            // lbForPointsOpponent
            // 
            this.lbForPointsOpponent.AutoSize = true;
            this.lbForPointsOpponent.Location = new System.Drawing.Point(13, 275);
            this.lbForPointsOpponent.Name = "lbForPointsOpponent";
            this.lbForPointsOpponent.Size = new System.Drawing.Size(82, 13);
            this.lbForPointsOpponent.TabIndex = 12;
            this.lbForPointsOpponent.Text = "Gegner Punkte:";
            // 
            // lbPointsOpponent
            // 
            this.lbPointsOpponent.AutoSize = true;
            this.lbPointsOpponent.Location = new System.Drawing.Point(114, 275);
            this.lbPointsOpponent.Name = "lbPointsOpponent";
            this.lbPointsOpponent.Size = new System.Drawing.Size(41, 13);
            this.lbPointsOpponent.TabIndex = 13;
            this.lbPointsOpponent.Text = "Punkte";
            // 
            // btSendPlay
            // 
            this.btSendPlay.Location = new System.Drawing.Point(13, 291);
            this.btSendPlay.Name = "btSendPlay";
            this.btSendPlay.Size = new System.Drawing.Size(75, 23);
            this.btSendPlay.TabIndex = 14;
            this.btSendPlay.Text = "Spielen!";
            this.btSendPlay.UseVisualStyleBackColor = true;
            // 
            // QuizDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 592);
            this.Controls.Add(this.btSendPlay);
            this.Controls.Add(this.lbPointsOpponent);
            this.Controls.Add(this.lbForPointsOpponent);
            this.Controls.Add(this.lbPointsUser);
            this.Controls.Add(this.lbForPointsUser);
            this.Controls.Add(this.lbChallenges);
            this.Controls.Add(this.listBoxChallenges);
            this.Controls.Add(this.btSendLogin);
            this.Controls.Add(this.tbPassword);
            this.Controls.Add(this.tbUsername);
            this.Controls.Add(this.lbQuestion);
            this.Controls.Add(this.btAnswerD);
            this.Controls.Add(this.btAnswerC);
            this.Controls.Add(this.btAnswerB);
            this.Controls.Add(this.btAnswerA);
            this.Name = "QuizDialog";
            this.Text = "KuckuQuiz";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btAnswerA;
        private System.Windows.Forms.Button btAnswerB;
        private System.Windows.Forms.Button btAnswerC;
        private System.Windows.Forms.Button btAnswerD;
        private System.Windows.Forms.Label lbQuestion;
        private System.Windows.Forms.TextBox tbUsername;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.Button btSendLogin;
        private System.Windows.Forms.ListBox listBoxChallenges;
        private System.Windows.Forms.Label lbChallenges;
        private System.Windows.Forms.Label lbForPointsUser;
        private System.Windows.Forms.Label lbPointsUser;
        private System.Windows.Forms.Label lbForPointsOpponent;
        private System.Windows.Forms.Label lbPointsOpponent;
        private System.Windows.Forms.Button btSendPlay;
    }
}