﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KuckuQuiz
{
    public partial class QuizDialog : Form
    {
        public QuizDialog()
        {
            InitializeComponent();
        }

        private void tbUsername_Click(object sender, EventArgs e)
        {
            if (tbUsername.Text == "Username")
            {
                tbUsername.Text = "";
            }
        }

        private void tbPassword_Click(object sender, EventArgs e)
        {
            if (tbPassword.Text == "Passwort")
            {
                tbPassword.Text = "";
            }
        }

        private void tbPassword_Leave(object sender, EventArgs e)
        {
            if (tbPassword.Text == "")
            {
                tbPassword.Text = "Passwort";
            }
        }

        private void tbUsername_Leave(object sender, EventArgs e)
        {
            if (tbUsername.Text == "")
            {
                tbUsername.Text = "Username";
            }
        }

        private void tbPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Return) && tbUsername.Text != "Username")
            {
                MessageBox.Show("Enter pressed");
            }
        }

        private void listBoxChallenges_Enter(object sender, EventArgs e)
        {
            //i-net-abfrage wv punkte beide haben, alle anderen daten nur speichern aber die beiden auch anzeigen!
            var client = new WebClient();
            string getOpponents = client.DownloadString("http://www.michaelkuckuk.com/de/KuckuCode/KuckuQuiz/request.php?purpose=getOpponents");
            getOpponents.Split(new Char[] { });
        }
    }
}
