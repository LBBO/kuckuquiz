﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Net;

namespace KuckuQuiz
{
    class Logger
    {
        private int loggerLevel;
        private string name;
        private string logPfad;
        private string user = Environment.UserName;
        private DateTime now = DateTime.Now;
        private DateTime today = DateTime.Today;

        public Logger(int level, string programmname)
        {
            this.loggerLevel = level;
            this.name = programmname;
            this.logPfad = @"c:\Users/" + this.user + "/Documents/KuckuCode/Logs/" + this.name.Replace(" ", "_");
            createLogfile();
        }

        private void createLogfile()
        {
            if (!System.IO.File.Exists(this.logPfad))
            {
                System.IO.Directory.CreateDirectory(this.logPfad);
            }
            this.logPfad = this.logPfad + "/" + today.ToString("d") + ".log";
            if (!System.IO.File.Exists(this.logPfad))
            {
                StreamWriter newLog = File.CreateText(this.logPfad);
                newLog.WriteLine("Log von " + today.DayOfWeek + ", dem " + today.ToString("d") +
                    " (sry für das Englisch)");
                newLog.WriteLine("Logstufe " + this.loggerLevel);
                newLog.WriteLine();
                newLog.WriteLine();
                newLog.Close();
            }
        }

        public void Error(string message)
        {
            StackFrame stackFrame = new StackFrame(1, true);

            string method = stackFrame.GetMethod().ToString();
            int line = stackFrame.GetFileLineNumber();
            using (StreamWriter log = new StreamWriter(this.logPfad, true))
            {
                log.WriteLine(now + ";   ERROR: " + message + " at Line " + line +
                    ", Method: " + method);
                log.Close();
            }
            string error_mit_leerzeichen = now + ";   ERROR: " + message + " in " + this.name + " at Line " + line +
                ", Method: " + method;
            string error_mit_doppelpunkt = error_mit_leerzeichen.Replace(" ", "_");
            string error = error_mit_doppelpunkt.Replace(":", ".");
            var client = new WebClient();
            string error_gesendet = client.DownloadString("http://localhost:8080/homepage/de/" +
                "KuckuCode/Backup/error.php?error=" +
                error);
        }

        public void Warning(string message)
        {
            if (loggerLevel > 1)
            {
                StackFrame stackFrame = new StackFrame(1, true);

                string method = stackFrame.GetMethod().ToString();
                int line = stackFrame.GetFileLineNumber();
                using (StreamWriter log = new StreamWriter(this.logPfad, true))
                {
                    log.WriteLine(now + ";   WARNUNG: " + message + " at Line " + line +
                        ", Method: " + method);
                    log.Close();
                }
            }
        }

        public void Info(string message)
        {
            if (loggerLevel > 2)
            {
                StackFrame stackFrame = new StackFrame(1, true);

                string method = stackFrame.GetMethod().ToString();
                int line = stackFrame.GetFileLineNumber();
                using (StreamWriter log = new StreamWriter(this.logPfad, true))
                {
                    log.WriteLine(now + ";   INFO: " + message + " at Line " + line +
                        ", Method: " + method);
                    log.Close();
                }
            }
        }
    }
}

