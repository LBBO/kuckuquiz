﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Net;
using System.Windows.Forms;

namespace KuckuQuiz
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new QuizDialog());
        }
    }
    //static void Main(string[] args)
    //{

    //    //1 = Error, 2 = Error und Warnung, 3 = Alles
    //    Logger logger = new Logger(3, "Programmname"); //MUSS ANGEPASST WERDEN!!!
    //    QuizDialog test = new QuizDialog();
    //    Application.Run(test);

    //    Console.ReadKey();
    //}

}
