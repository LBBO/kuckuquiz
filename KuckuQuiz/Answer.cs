﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Diagnostics;

namespace KuckuQuiz
{
    class Answer
    {
        private string foldername;
        private List<string> answers;
        private bool success;
        private List<string> handle;
        private Logger logger = new Logger(3, "KuckuQuiz.Answer");

        public Answer(string foldername)
        {
            try
            {
                var client = new WebClient();
                string antworten_text = client.DownloadString("http://www.michaelkuckuk.com/de/KuckuCode/KuckuQuiz/antworten.txt");
                StreamWriter antworten = File.CreateText(foldername + "/antworten.txt");
                antworten.Write(antworten_text);
                antworten.Close();
            }
            catch
            {
                logger.Warning("Antworten konnten nicht gedownloadet werden!");
            }
            try
            {
                using (StreamReader answersInput = new StreamReader(foldername + "/antworten.txt"))
                {
                    while (!answersInput.EndOfStream)
                    {
                        answers.Add(answersInput.ReadLine());
                    }
                }
                success = true;
                logger.Info("Antworten wurden ausgelesen");
            }
            catch
            {
                logger.Error("Antworten wurden nicht ausgelesen!");
                success = false;
            }
        }

        public bool Success
        {
            get
            {
                return this.success;
            }
        }

        public bool Next(int index)
        {
            handle.Clear();

            bool result = false;
            try
            {
                for (int i = index * 4 + 4; i > i * 4; i--)
                {
                    this.handle.Add(this.answers[index]);
                    this.answers.RemoveAt(index);
                }
                result = true;
            }
            catch (IndexOutOfRangeException)
            {
                logger.Error("IndexOutOfRange!!!!! antwort mit index " + index + "existiert nicht!");
                result = false;
            }
            return result;
        }

        public List<string> Handle
        {
            get
            {
                return this.handle;
            }
        }
    }
}