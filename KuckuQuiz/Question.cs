﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Diagnostics;

namespace KuckuQuiz
{
    class Question
    {
        private string foldername;
        private List<string> questions;
        private bool success;
        private string handle;
        private Logger logger = new Logger(3, "KuckuQuiz_Question");

        public Question(string foldername)
        {
            try
            {
                var client = new WebClient();
                string fragen_text = client.DownloadString("http://www.michaelkuckuk.com/de/KuckuCode/KuckuQuiz/fragen.txt");
                StreamWriter fragen = File.CreateText(foldername + "/fragen.txt");
                fragen.Write(fragen_text);
                fragen.Close();
            }
            catch
            {
                logger.Warning("Fragen konnten nicht gedownloadet werden!");
            }
            try
            {
                using (StreamReader questionsInput = new StreamReader(foldername + "/fragen.txt"))
                {
                    while (!questionsInput.EndOfStream)
                    {
                        questions.Add(questionsInput.ReadLine());
                    }
                }
                success = true;
                logger.Info("Fragen wurden ausgelesen");
            }
            catch
            {
                logger.Error("Fragen wurden nicht ausgelesen!");
                success = false;
            }
        }

        public bool Success
        {
            get
            {
                return this.success;
            }
        }

        public int Count
        {
            get
            {
                return this.questions.Count();
            }
        }

        public bool Next(int index)
        {
            bool result = false;
            try
            {
                this.handle = this.questions[index];
                this.questions.RemoveAt(index);
                result = true;
            }
            catch(IndexOutOfRangeException)
            {
                logger.Error("IndexOutOfRange!!!!! Frage mit index " + index + "existiert nicht!");
                result = false;
            }
            return result;
        }

        public string Handle
        {
            get
            {
                return this.handle;
            }
        }
    }
}