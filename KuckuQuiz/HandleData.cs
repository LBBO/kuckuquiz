﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Net;

namespace KuckuQuiz
{
    class HandleData
    {
        private Random Rand = new Random();
        private string user = Environment.UserName;
        private string foldername;
        private Logger logger = new Logger(3, "KuckuQuiz_HandleData");
        private bool spielGeht;
        private Question question;
        private Answer answer;
        private Categories category;
        private string handleQuestion;
        private List<string> handleAnswer;
        private string[] possibleAnswers = new string[] { "A", "B", "C", "D" };
        private string right;
        private string handleCategory;
        

        public HandleData()
        {
            this.foldername = @"c:/Users/" + user + "/Documents/KuckuCode/KuckuGames/KuckuQuiz";
            this.spielGeht = false;
            question = new Question(foldername);
            answer = new Answer(foldername);
            category = new Categories(foldername);
            if (!(question.Success && answer.Success && category.Success))
            {
                Console.WriteLine("Eine der notwendigen Dateien konnte nicht geladen werden, darum ist das Programm nun zu Ende.");
                this.spielGeht = false;
            }
        }

        public bool SpielGeht
        {
            get
            {
                return this.spielGeht;
            }
        }

        public bool NewValues()
        {
            bool result = false; ;
            try
            {
                int randomNumber = Rand.Next(0, this.question.Count);
                if (this.question.Next(randomNumber) && this.answer.Next(randomNumber) && this.category.Next(randomNumber))
                {
                    this.handleQuestion = this.question.Handle;
                    this.handleCategory = this.category.Handle;
                    List<string> rawHandleAnswer = this.answer.Handle;
                }
                else
                {
                    Console.WriteLine("Interner Fehler");
                    spielGeht = false;
                }
                result = true;
            }
            catch
            {
                result = false;
            }
            return result;
        }
    }
}
